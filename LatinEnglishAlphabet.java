// Project: https://rosettacode.org/wiki/Idiomatically_determine_all_the_lowercase_and_uppercase_letters
public class LatinEnglishAlphabet {
    public static void main(String[] args) {
        // Hex codes gotten from https://www.asciitable.com/
        System.out.printf("Hardware Architecture: %s\nOperating System: %s\n", 
            System.getProperty("os.arch"),
            System.getProperty("os.name") + " " + System.getProperty("os.version")
        );
        // Uppercase letters
        int l = 0x41;
        while (l <= 0x5a) {
            System.out.printf("%c", l);
            l++;
        }
        // Lowercase letters
        l = 0x61;
        while (l <= 0x7a) {
            System.out.printf("%c", l);
            l++;
        }
        System.out.println();
    }
}
